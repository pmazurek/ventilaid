# VentilAid

VentilAid - open source ventilator, that can be made anywhere locally
Files names are in Polish - we will fix it and change to English untill 24-25th of March 2020.

The parts required to assemble the VentilAid are divided into two groups:
1) Printable parts
2) Parts that are mass-produced worldwide

As for the first group, the instructions are the following:
- Print the bellow out of elastic material, for instance TPU. For best results please use material of hardness 80 in Shore A scale. When slicing, the best settings appear to be 1 loop with 0.6-0.7 extrusion width. Apply "Vase mode" whenever it is possible.

- Other 3D printed parts should be sliced with following settings: 2 loops, approx. 15% infill density, layer height from 0.2 mm to 0.5 mm.

The second group contains parts, which are ought to be purchased from local supplier. The list is available below:
- Double acting pneumatic actuator with 16 mm piston and 100-200 mm stroke (the stroke length is responsible for "breath" capacity)
- 12 V solenoid oneumatic valve with adjustable air outlet (mono- or bistable)
- 12 V DC power supply - almost any kind will work
- Standarized pneumatic pressure regulator with approx. 2 bar on the output.
- Source of pneumatic pressure - Ventilaid should work even on compressors for hobbyists

The check-valve and the pressure controller require a soft, elastic discs with hardness of approx. 40 Shore A (or less) and maximal thickness of 1mm - if the supply is low one can make on out of old bike tire tube.


# P.Mazurek
## Issues

### Ender-3 update to print elastomers:
1. https://www.youtube.com/watch?v=az7pysnV_MY

https://www.thingiverse.com/thing:3072475
https://www.thingiverse.com/thing:2144277 - Flexible Filament Extruder Upgrade for Creality CR-7, CR-10, Ender 2, Ender 3

https://www.amazon.com/gp/product/B079P92HN9/ref=as_li_qf_asin_il_tl?ie=UTF8&tag=chuchellselec-20&creative=9325&linkCode=as2&creativeASIN=B079P92HN9&linkId=7c8afd779958ddf7468b67e617a9cd0c - Capricorn Bowden PTFE Tubing XS Series 1 Meter for 1.75mm Filament (Genuine Capricorn Premium Tubing


2. https://www.youtube.com/watch?v=1GNDxvxpWr8

EZR Extruder:
SeeMeCNC: http://bit.ly/2NYGhTi
or
MatterHackers: https://bit.ly/2xWSIEo


### Doubts
 - "Hardness 80 in Shore A scale." (what does mean and how to check if - there is no such informations in filament shop) -Done
 - What is format STEPS, SLDPRT (how to convert to some format readable by ender-3) - (https://www.onshape.com/ can convert SLDPRT to stl, or from STEPS https://www.makexyz.com/convert/step-to-stl work)